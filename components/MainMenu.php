<?php

/**
 * MainMenu is a widget displaying main menu items.
 *
 * The menu items are displayed as an HTML list. One of the items
 * may be set as active, which could add an "active" CSS class to the rendered item.
 *
 * To use this widget, specify the "items" property with an array of
 * the menu items to be displayed. Each item should be an array with
 * the following elements:
 * - visible: boolean, whether this item is visible;
 * - label: string, label of this menu item. Make sure you HTML-encode it if needed;
 * - url: string|array, the URL that this item leads to. Use a string to
 *   represent a static URL, while an array for constructing a dynamic one.
 * - pattern: array, optional. This is used to determine if the item is active.
 *   The first element refers to the route of the request, while the rest
 *   name-value pairs representing the GET parameters to be matched with.
 *   When the route does not contain the action part, it is treated
 *   as a controller ID and will match all actions of the controller.
 *   If pattern is not given, the url array will be used instead.
 */
class MainMenu extends CWidget
{

    private $items;

    public function init()
    {
        if (Yii::app()->user->isGuest)
        {
            $this->initGuestMenu();
        } else
        {
            $this->initUserMenu();
        }
    }

    private function initGuestMenu()
    {
        $this->items[] = array('label' => 'Home', 'url' => array('/site/index'));
        $this->items[] = array('label' => 'About', 'url' => array('/site/page', 'view' => 'about'));
        $this->items[] = array('label' => 'Contact', 'url' => array('/site/contact'));
        $this->items[] = array('label' => 'Login', 'url' => array('/site/login'));
    }
    
    private function initUserMenu()
    {
        $this->items[] = array('label' => 'Home', 'url' => array('/site/index'));
        $this->items[] = array('label' => 'About', 'url' => array('/site/page', 'view' => 'about'));
        $this->items[] = array('label' => 'Contact', 'url' => array('/site/contact'));
        $this->items[] = array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'));
    }

    public function run()
    {
        $this->widget('zii.widgets.CMenu', array('items' => $this->items));
    }

}
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GlobalFunctions
 *
 * @author marius
 */
class RbacHelper {

	static public function deservesProjectVendorRole(User $user) {
		return self::isAuthenticated() 
				&& self::isActivated($user) 
				&& self::isNotDisabled($user)
				&& $user->is_project_vendor;
	}

	static public function deservesServiceProviderRole(User $user) {
		return self::isAuthenticated() 
				&& self::isActivated($user) 
				&& self::isNotDisabled($user)
				&& !$user->is_project_vendor;
	}

	static public function deservesAuthenticatedRole(User $user) {
		return self::isAuthenticated() 
				&& !self::isActivated($user)
				&& self::isNotDisabled($user);
	}

	static public function isAuthenticated() {
		return !Yii::app()->user->isGuest;
	}

	static public function isActivated(User $user) {
		return !empty($user->activation_time);
	}

	static public function isNotDisabled(User $user) {
		return $user->enabled;
	}

}

<?php
/**
 * Description of FormInputRenderer
 *
 * @author marius
 */
class FormInputRenderer {
    
    private $form;
    private $errors;

    public function __construct(CForm $form) {
        $this->form = $form;
        $this->errors = $form->getModel()->getErrors();
    }
    
    public function render($attribute, $htmlOptions=array()) {
        $content = $this->form[$attribute]->render();
        if (!empty($htmlOptions['class'])) {
            $classString = $htmlOptions['class'];
            unset($htmlOptions['class']);
            $classAttributes = $this->getHtmlClassAttributes($attribute, $classString);
        } else {
            $classAttributes = $this->getHtmlClassAttributes($attribute);
        }
        $htmlOptions['class'] = $classAttributes;
        return CHtml::tag('div', $htmlOptions, $content);
    }
    
    private function getHtmlClassAttributes($attribute, $classString='') {
        $inputClass = $this->getInputClass($attribute);
        $errorClass = $this->getErrorClass($attribute);
        $classString = (!empty($classString))? "{$classString} {$inputClass}" : $inputClass;
        $classString = (!empty($errorClass))? "{$classString} {$errorClass}" : $classString;
        return $classString;
    }
    
    private function getInputClass($attribute) {
        $inputElement = $this->form[$attribute];
        switch ($inputElement->type) {
            case 'text':
            case 'password':
            case 'textarea':
            case 'file':
            case 'zii.widgets.jui.CJuiAutoComplete':
                $type = 'ym-fbox ym-fbox-text';
                break;
            case 'radio':
            case 'checkbox':
            case 'radiolist':
            case 'checkboxlist':
                $type = 'ym-fbox ym-fbox-check';
                break;
            case 'listbox':
            case 'dropdownlist':
                $type = 'ym-fbox ym-fbox-select';
                break;
            case 'hidden':
                $type = 'ym-hideme';
                break;
            default:
                $type = 'ym-unknown';
                break;
        }
        return $type;
    }
    
    private function getErrorClass($attribute) {
        return (isset($this->errors[$attribute]))? 'error' : '';
    }
}
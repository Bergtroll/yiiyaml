<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormFactory
 *
 * @author marius
 */
class FormFactory {

	public $formCssClass = 'ym-form';
	public $errorMessageCssClass = 'ym-message';
	public $enableAjaxValidation = true;
	private $_controller;
	private $_model;

	public function __construct(CController $controller, CActiveRecord $model) {
		$this->_controller = $controller;
		$this->_model = $model;
	}

	public function createForm() {
		$formConfig = $this->getFormConfig();
		$form = new CForm($formConfig, $this->_model);
		$form->activeForm = array(
			'class' => 'CActiveForm',
			'id' => $this->getFormId(),
			'htmlOptions' => array('class' => $this->formCssClass),
			'clientOptions' => array(
				'errorCssClass' => 'ym-error',
				'successCssClass' => null,
				'validationUrl' => $this->_controller->createUrl('validateModel'),
			),
			'enableAjaxValidation' => $this->enableAjaxValidation,
			'errorMessageCssClass' => $this->errorMessageCssClass,
		);
		$form->action = $this->getFormAction();
		return $form;
	}

	public function getFormConfig() {
		$modelClass = get_class($this->_model);
		$modelClass[0] = strtolower($modelClass[0]);
		$config = sprintf('application.views.%s.formConfig', $modelClass);
		return $config;
	}

	public function getFormId() {
		$formId = $this->getModelClassId() . '-form';
		return $formId;
	}

	public function getModelClassId() {
		$modelClass = get_class($this->_model);
		return trim(strtolower(str_replace('_', '-', preg_replace('/(?<![A-Z])[A-Z]/', '-\0', $modelClass))), '-');
	}

	public function getFormAction() {
		$model = $this->_model;
		$controllerId = $this->_controller->getId();
		if ($model->isNewRecord) {
			$action = CHtml::normalizeUrl(array($controllerId . '/create'));
		} else {
			$action = CHtml::normalizeUrl(array($controllerId . '/update', 'id' => $model->id));
		}
		return $action;
	}

}

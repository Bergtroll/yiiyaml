<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of YamlCssClassMapper
 *
 * @author marius
 */
class YamlCssClassMapper {
    
    // Input form layout classes
    const INPUT = 'ym-fbox-text';
    const TEXTAREA = 'ym-fbox-text';
    const SELECT = 'ym-fbox-select';
    const CHECKBOX = 'ym-fbox-check';
    const RADIO = 'ym-fbox-check';
    const BUTTON = 'ym-fbox-button';
    
    // Extended label class
    const LABEL = 'ym-label';
    
    // Highlighter classes
    const MESSAGE = 'ym-message';
    const ERROR = 'ym-error';
    const REQUIRED = 'ym-required';
    const HIDDEN = 'ym-hideme';
    
    static public function getInputElementCssClass($type='text') {
        switch ($type) {
            case 'text':
            case 'password':
            case 'textarea':
            case 'file':
            case 'zii.widgets.jui.CJuiAutoComplete':
            case 'zii.widgets.jui.CJuiDatePicker':
                $type = self::INPUT;
                break;
            case 'radio':
            case 'checkbox':
            case 'radiolist':
            case 'checkboxlist':
                $type = self::CHECKBOX;
                break;
            case 'listbox':
            case 'dropdownlist':
                $type = self::SELECT;
                break;
            case 'hidden':
                $type = self::HIDDEN;
                break;
			case 'button':
                $type = self::BUTTON;
                break;
            default:
                $type = self::INPUT;
                break;
        }
        return $type;
    }
    
    static public function getErrorClass() {
        return self::ERROR;
    }
    
    static public function getMessageClass() {
        return self::MESSAGE;
    }
    
    static public function getRequiredClass() {
        return self::REQUIRED;
    }
    
    static public function getHiddenClass() {
        return self::HIDDEN;
    }
    
}


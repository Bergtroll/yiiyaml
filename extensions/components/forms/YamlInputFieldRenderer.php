<?php

/**
 * Description of FormInputRenderer
 *
 * @author marius
 */
class YamlInputFieldRenderer {

    public $layout = "{error}\n{label}\n{input}";
    private $errors;
    private $classMapper;

    public function __construct() {
        $this->classMapper = new YamlCssClassMapper;
    }

    public function render($element, $htmlOptions = array()) {
        $content = $this->getContent($element);
        $htmlOptions['class'] = $this->mergeCssClassAttribute($element, $htmlOptions);
        return CHtml::tag('div', $htmlOptions, $content);
    }

    private function getContent($element) {
        $label = $element->renderLabel();
        $output = array(
            '{label}' => $label,
            '{input}' => $element->renderInput(),
            '{error}' => $element->getParent()->showErrorSummary ? '' : $element->renderError(),
        );
        return strtr($this->layout, $output);
    }

    private function mergeCssClassAttribute($element, $htmlOptions = array()) {
        if (!empty($htmlOptions['class'])) {
            $classString = $htmlOptions['class'];
            unset($htmlOptions['class']);
        } else {
            $classString = '';
        }
        $classString = $this->getHtmlClassAttributes($element, $classString);
        return $classString;
    }

    private function getHtmlClassAttributes($element, $classString = '') {
        $inputClass = $this->classMapper->getInputElementCssClass($element);
        $errorClass = $this->getErrorClass($element);
        $classString = (!empty($classString)) ? "{$classString} {$inputClass}" : $inputClass;
        $classString = (!empty($errorClass)) ? "{$classString} {$errorClass}" : $classString;
        return $classString;
    }

    private function getErrorClass($attribute) {
        if (isset($this->errors[$attribute])) {
            return $this->classMapper->getErrorClass();
        } else {
            return '';
        }
    }

}
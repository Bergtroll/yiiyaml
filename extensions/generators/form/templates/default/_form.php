<?php echo '<?php\n\n'; ?>
$buttons = $form->getButtons(); 
$model = $form->getModel();
$renderer = new FormInputRenderer($form);
<?php 
echo '?>'; 
echo '<?php echo $form->renderBegin();'
?>
    <fieldset>
        <legend><?php echo '<?php echo $form->title; ?>'?></legend>
<?php foreach ($elements as $name => $element): ?>
        <div class="<?php $this->renderContainerCss($element); ?>">
            <?php echo '$this->form[' . $name . ']->renderError()'; ?>
            <?php echo '$this->form[' . $name . ']->renderLabel()'; ?>
            <?php echo '$this->form[' . $name . ']->renderInput()'; ?>
        </div>
<?php endforeach; ?>
    </fieldset>
<?php echo '<?php echo $form->renderEnd(); ?>'?>
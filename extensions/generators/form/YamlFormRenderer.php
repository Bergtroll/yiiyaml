<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of YamlFormRenderer
 *
 * @author marius
 */
class YamlFormRenderer {

    public $form;
    public $fieldRenderer;
    public $renderFieldset = true;
    public $renderLegend = true;
    public $renderButtons = true;

    public function __construct(CForm $form) {
        CHtml::$afterRequiredLabel = '<sup class="ym-required">*</sup>';
        CHtml::$requiredCss = null;
        $this->form = $form;
        $this->fieldRenderer = new YamlInputFieldRenderer();
    }

    public function render() {
        echo $this->form->renderBegin();
        $this->renderContent();
        echo $this->form->renderEnd();
    }

    public function renderContent() {
        if ($this->renderFieldset) {
            echo CHtml::openTag('fieldset');
        }
        if ($this->renderLegend) {
            echo CHtml::tag('legend', array(), $this->form->title);
        }
        $this->renderInputElements();
        if ($this->renderButtons) {
            $this->renderButtons();
        }
        if ($this->renderFieldset) {
            echo CHtml::closeTag('fieldset');
        }
    }

    public function renderLegend() {
        
    }

    public function renderInputElements() {
        $elements = $this->form->elements;
        foreach ($elements as $element) {
            echo $this->fieldRenderer->render($element);
        }
    }

    public function renderInputElement() {
        
    }

    public function renderButtons() {
        $model = $this->form->getModel();
        $buttons = $this->form->getButtons();
        $button = ($model->isNewRecord) ? $buttons['create'] : $buttons['update'];
        echo CHtml::tag('div', array('class' => 'ym-fbox-button'), $button);
    }

    public function renderFormEnd() {
        
    }

}


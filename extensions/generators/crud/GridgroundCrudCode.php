<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Yii::import('system.gii.generators.crud.CrudCode');

/**
 * Description of GridgroundCrudCode
 *
 * @author marius
 */
class GridgroundCrudCode extends CrudCode {

	private $_formConfig;
	
	public function prepare() {
		$this->files = array();
		$this->prepareController();
		$this->prepareCrudViews();
		$this->setupFormConfiguration();
		$this->prepareFormConfig();
		$this->prepareYamlForm();
	}

	public function prepareController() {
		$controllerTemplateFile = $this->templatePath . DIRECTORY_SEPARATOR . 'controller.php';
		$this->files[] = new CCodeFile(
						$this->controllerFile,
						$this->render($controllerTemplateFile)
		);
	}

	public function prepareCrudViews() {
		$files = scandir($this->templatePath);
		foreach ($files as $file) {
			if (is_file($this->templatePath . '/' . $file) && CFileHelper::getExtension($file) === 'php' && $file !== 'controller.php' && $file !== '_form.php') {
				$this->files[] = new CCodeFile(
								$this->viewPath . DIRECTORY_SEPARATOR . $file,
								$this->render($this->templatePath . '/' . $file)
				);
			}
		}
	}

	public function setupFormConfiguration() {
		$table = $this->getTableSchema();
		$foreignKeys = array();
		foreach ($table->foreignKeys as $column => $fk) {
			$foreignKeys[$column] = array(
				$this->generateClassName($fk[0]),
				$fk[1],
			);
		};
		$formConfigRenderer = new FormConfigRenderer();
		$formConfigRenderer->tableSchema = $table;
		$formConfigRenderer->foreignKeys = $foreignKeys;
		$formConfigRenderer->tableModelClassName = $this->getModelClass();
		$this->_formConfig = $formConfigRenderer->getConfig();
	}

	public function prepareFormConfig() {
		$path = Yii::getPathOfAlias('application.views.' . lcfirst($this->getModelClass()));
		$path .= '/formConfig.php';
		$config = "<?php\n\n";
        $config .= GeneratorUtils::convertToConfigFileString($this->_formConfig);
		$this->files[] = new CCodeFile($path, $config);
	}

	public function prepareYamlForm() {
		$params = array(
			'elements' => $this->_formConfig->itemAt('elements'),
		);
		$file = '_form.php';
		$this->files[] = new CCodeFile(
				$this->viewPath . DIRECTORY_SEPARATOR . $file, 
				$this->render($this->templatePath . '/' . $file, $params)
		);
	}
	
	public function renderContainerCss($type) {
		return YamlCssClassMapper::getInputElementCssClass($type);
	}

}
<?php

class GridgroundCrudGenerator extends CCodeGenerator {

	public $codeModel = 'ext.gridground.generators.gridgroundCrud.GridgroundCrudCode';
	
	protected function prepare() {
		$model = parent::prepare();
		$model->baseControllerClass = 'CrudController';
		return $model;
	}

}
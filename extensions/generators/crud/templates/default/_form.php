<?php echo '<?php' . PHP_EOL; ?>
$buttons = $form->getButtons(); 
$model = $form->getModel();
$errors = $model->getErrors();
CHtml::$afterRequiredLabel = '<sup class="ym-required">*</sup>';
CHtml::$requiredCss = null;
CHtml::$errorCss = null;
<?php 
echo '?>' . PHP_EOL;
echo '<?php echo $form->renderBegin(); ?>' . PHP_EOL;
?>
    <fieldset>
        <legend><?php echo '<?php echo $form->title; ?>'?></legend>
		<?php echo '<?php' . PHP_EOL; ?>
		if (!empty($errors)) {
			$widget = $form->getActiveFormWidget();
			echo $widget->errorSummary($model);
		} 
		<?php echo '?>' . PHP_EOL; ?>
<?php foreach ($elements as $name => $element): ?>
		<?php $errorClass = '<?php if (isset($errors[\'' . $name . '\'])) echo \' ym-error\'; ?>'; ?>
		
        <div class="<?php echo $this->renderContainerCss($element['type']) . $errorClass; ?>">
            <?php echo '<?php echo $form[\'' . $name . '\']->renderError() . PHP_EOL; ?>' . PHP_EOL; ?>
            <?php echo '<?php echo $form[\'' . $name . '\']->renderLabel() . PHP_EOL; ?>' . PHP_EOL; ?>
            <?php echo '<?php echo $form[\'' . $name . '\']->renderInput() . PHP_EOL; ?>' . PHP_EOL; ?>
        </div>
<?php endforeach; ?>
		<div class="<?php echo $this->renderContainerCss('button'); ?>">
			<?php echo '<?php echo ($model->isNewRecord)? $buttons[\'create\'] : $buttons[\'update\']; ?>' . PHP_EOL; ?>
		</div>
    </fieldset>
<?php echo '<?php echo $form->renderEnd(); ?>'?>
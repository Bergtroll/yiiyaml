<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php
echo "<?php\n";
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	Yii::t('crud', 'Create'),
);\n";
?>

$this->menu=array(
	array('label'=>Yii::t('crud', 'List') . ' <?php echo $this->modelClass; ?>', 'url'=>array('index')),
	array('label'=>Yii::t('crud', 'Manage') . ' <?php echo $this->modelClass; ?>', 'url'=>array('admin')),
);
?>

<h1><?php echo '<?php Yii::t(\'crud\', \'Create\'); ?> ' . $this->modelClass; ?></h1>

<?php echo "<?php echo \$this->renderPartial('_form', array('form'=>\$form)); ?>"; ?>

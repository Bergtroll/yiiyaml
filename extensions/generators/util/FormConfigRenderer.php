<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormConfigRenderer
 *
 * @author marius
 */
class FormConfigRenderer {

    public $foreignKeys = array();
    public $tableSchema;
    public $tableModelClassName;
    public $localesList = "array('de' => Yii::t('crud', 'German'), 'en' => Yii::t('crud', 'English'))";
    public $minSizeForTextArea = 51;
    private $_config;

	public function getConfig() {
		$this->init();
		return $this->_config;
	}

    public function init() {
        $this->_config = new CConfiguration();
        $this->setFormConfigTitle();
        $this->setFormConfigElements();
        $this->setFormConfigButtons();
    }

    public function setFormConfigTitle() {
        $element = "Yii::t('%s', '%s') . ' ' . Yii::t('crud', 'form')";
        $element = sprintf($element, $this->tableSchema->name, $this->tableModelClassName);
        $this->_config->add('title', $element);
    }

    public function setFormConfigElements() {
        $config = new CConfiguration;
        $columns = $this->tableSchema->columns;
        foreach ($columns as $columnName => $columnSchema) {
            $columnConfig = $this->createColumnConfig($columnSchema);
            $config->add($columnName, $columnConfig);
        }
        $this->_config->add('elements', $config->toArray());
    }

    /**
     *
     * @param string $columnName
     * @param CDbColumnSchema $columnSchema 
     */
    public function createColumnConfig(CDbColumnSchema $columnSchema) {
        if ($columnSchema->isPrimaryKey) {
            $config = $this->createPrimaryKeyColumnConfig($columnSchema);
        } elseif ($columnSchema->isForeignKey) {
            $config = $this->createForeignKeyColumnConfig($columnSchema);
        } else {
            switch ($columnSchema->type) {
                case "integer":
                case "double":
                    $config = $this->createNumberColumnConfig($columnSchema);
                    break;
                case "boolean" :
                    $config = $this->createBooleanColumnConfig();
                    break;
                case "string":
                default:
                    $config = $this->createStringColumnConfig($columnSchema);
            }
        }
        return $config;
    }

    public function createPrimaryKeyColumnConfig(CDbColumnSchema $columnSchema) {
        return $this->createNumberColumnConfig($columnSchema);
    }

    public function createForeignKeyColumnConfig(CDbColumnSchema $columnSchema) {
        $foreignClassName = $this->foreignKeys[$columnSchema->name][0];
        $foreignColumnName = $this->foreignKeys[$columnSchema->name][1];
        $modelList = sprintf('%s::model()->findAll()', $foreignClassName);
        $listData = 'CHtml::listData(%s, %s, %s)';
        $listData = sprintf($listData, $modelList, $foreignColumnName, $foreignColumnName);
        $config = array(
            'type' => 'dropdownlist',
            'items' => $listData,
            'prompt' => "Yii::t('crud', '-- Choose --')",
        );
        return $config;
    }

    public function createNumberColumnConfig(CDbColumnSchema $columnSchema) {
        $config = array(
            'type' => 'text',
            'size' => 10,
        );
        if (!empty($columnSchema->size)) {
            $config['maxlength'] = $columnSchema->size;
        }
        return $config;
    }

    public function createBooleanColumnConfig() {
        return array(
            'type' => 'checkbox',
        );
    }

    public function createStringColumnConfig(CDbColumnSchema $columnSchema) {
        if ($this->isColumnOfDateType($columnSchema)) {
            $config = array(
                'type' => 'zii.widgets.jui.CJuiDatePicker',
                'options' => array(
                    'showAnim' => 'fold',
                    'language' => 'Yii::app()->getLocale()',
                ),
            );
        } elseif ($this->isColumnOfLocaleType($columnSchema)) {
            $config = array(
                'type' => 'dropdownlist',
                'items' => $this->localesList,
                'prompt' => "Yii::t('crud', '-- Choose --')",
            );
        } elseif ($this->isColumnOfNumericType($columnSchema)) {
            $config = $this->createNumberColumnConfig($columnSchema);
        } elseif (!empty($columnSchema->size) && $columnSchema->size < $this->minSizeForTextArea) {
            $config = array(
                'type' => 'text',
                'size' => 20,
                'maxlength' => $columnSchema->size,
            );
        } else {
            $config = array(
                'type' => 'textarea',
                'cols' => 50,
                'rows' => 10,
            );
        }
        return $config;
    }

    public function isColumnOfDateType(CDbColumnSchema $columnSchema) {
        $identifiers = array('date', 'time', 'day', 'month', 'year', 'birth');
        return StringUtil::stringContainsIdentifier($columnSchema->name, $identifiers);
    }

    public function isColumnOfLocaleType(CDbColumnSchema $columnSchema) {
        $identifiers = array('locale', 'language');
        return StringUtil::stringContainsIdentifier($columnSchema->name, $identifiers);
    }

    public function isColumnOfNumericType(CDbColumnSchema $columnSchema) {
        $identifiers = array('numeric', 'decimal');
        return StringUtil::stringContainsIdentifier($columnSchema->dbType, $identifiers);
    }

    public function setFormConfigButtons() {
        $element = array(
            'create' => array(
                'type' => 'submit',
                'label' => "Yii::t('crud', 'create')",
            ),
            'update' => array(
                'type' => 'submit',
                'label' => "Yii::t('crud', 'update')",
            )
        );
        $this->_config->add('buttons', $element);
    }

}


<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GeneratorUtils
 *
 * @author marius
 */
abstract class GeneratorUtils {
    
    static public function convertToConfigFileString(CConfiguration $config, $startIndent = 0) {
        $configArray = $config->toArray();
        $configString = str_repeat("\t", $startIndent) . "return array(\n";
        foreach ($configArray as $key => $value) {
            $configString .= self::convertConfigElement($key, $value, $startIndent + 1);
        }
        $configString .= str_repeat("\t", $startIndent) . ');';
        return $configString;
    }
    
    static public function convertConfigElement($key, $data, $startIndent) {
        $currentOutputString = '' . str_repeat("\t", $startIndent);
        if (is_array($data)) {
            $currentOutputString .= "'{$key}' => array(\n";
            foreach ($data as $childKey => $childData) {
                $currentOutputString .= self::convertConfigElement($childKey, $childData, $startIndent + 1);
            }
            $currentOutputString .= str_repeat("\t", $startIndent) . "),\n";
        } else {
            $isContainer = StringUtil::stringContainsIdentifier($data, array('::', '->'));
            if (!$isContainer) {
                $data = "'{$data}'";
            }
            $currentOutputString .= "'{$key}' => $data,";
            $currentOutputString .= "\n";
        }
        return $currentOutputString;
    }
}

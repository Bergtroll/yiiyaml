<?php

/**
 * Description of RecordFinder
 *
 * @author marius
 */
class RecordFinder {
    
    public static function findByAttributes($className, $attributes) {
        return CActiveRecord::model($className)->findByAttributes($attributes);
    }
    
}

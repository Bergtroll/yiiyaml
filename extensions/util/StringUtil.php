<?php

/**
 * Description of StringUtils
 *
 * @author marius
 */
class StringUtil {

    /**
     *
     * @param string $string
     * @param mixed $identifiers
     * @return type 
     */
    static public function stringContainsIdentifier($string, $identifiers) {
        $contains = false;
        if (is_array($identifiers)) {
            foreach ($identifiers as $identifier) {
                if (stripos($string, $identifier) !== false) {
                    $contains = true;
                    break;
                }
            }
        } else {
            $contains = (stripos($string, $identifiers) !== false);
        }
        return $contains;
    }

}

